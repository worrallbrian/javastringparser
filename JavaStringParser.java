import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.text.SimpleDateFormat;
import java.util.Date;

public class JavaStringParser {
	private static class WordStat {
		private String word;
		private int occurences;

		public WordStat(String word1, int occurences1) {
			this.word = word1;
			this.occurences = occurences1;
		}

		public String getWord() {
			return this.word;
		}

		public int getOccurences() {
			return this.occurences;
		}
	}

	private static String parseTextFile(String filename) throws IOException  {
		if ((filename == null) || (filename.equals(""))) {
			return ("");
		}

		String data = "";
		BufferedReader br = null;

		try {
			br = new BufferedReader(new FileReader(filename));
		} catch (Exception e) {
			System.out.println("ERROR READING FILE: " + e.toString());
			System.exit(1);
		}

		try {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
				line = br.readLine();
			}

			data = sb.toString();
			line = null;
			sb = null;
		} catch (Exception e) {
			br.close();
			System.out.println("ERROR READING FILE: " + e.toString());
			System.exit(1);
		} finally {
			br.close();
		}

		return data;
	}

	private static ArrayList<WordStat> parseWords(String para) {
		ArrayList<WordStat> word_list = new ArrayList<WordStat>();

		if ((para == null) || (para.equals(""))) {
			return word_list;
		}

		String[] words = para.split("\\s+");

		if ((words != null) && (words.length > 0)) {
			WordStat tempws = null;
			String temp = "";
			int i = 0;
			int j = 0;
			int isFound = 0;
			int occ = 1;

			for (i = 0; i < words.length; i++) {
				if (words[i] == null) {
					continue;
				}

				temp = (words[i]).trim().replaceAll("[^A-Za-z0-9]", "");

				if (temp.equals("")) {
					continue;
				}

				isFound = 0;

				if (word_list.size() > 0) {
					for (j = 0; j < word_list.size(); j++) {
						tempws = word_list.get(j);

						if ((tempws != null) && ((tempws.getWord()).equals(temp))) {
							isFound = 1;
							occ = tempws.getOccurences();

							if (occ > 0) {
								occ += 1;
							} else {
								occ = 1;
							}

							tempws = new WordStat(temp, occ);
							word_list.set(j, tempws);

							break;
						}
					}
				}

				if (isFound == 0) {
					tempws = new WordStat(temp, 1);
					word_list.add(tempws);
				}
			}

			tempws = null;
			temp = null;
		}

		words = null;

		return word_list;
	}

	/* Chop strings into 60 character lengths + newline character to avoid HTML line length overflows */
	private static String safeHTMLString(String ls) {
		String result = "";

		if ((ls != null) && (!ls.equals(""))) {
			int qlength = ls.length();
			int iter = (int) Math.ceil(qlength / 60);
			StringBuilder temp = new StringBuilder();
			int mi = 0;
			int i = 0;

			for (i = 0; i < iter; i++) {
				mi = i * 60;

				if (mi > qlength) {
					break;
				}

				temp.append(ls.substring(mi, Math.min(qlength, mi + qlength)));
				temp.append("\n");
			}

			result = temp.toString();
			temp = null;
		}

		return result;
	}

	private static String buildStatRow(WordStat ws, int is_odd) {
		StringBuilder data2 = new StringBuilder();

		if (ws != null) {
			String w = ws.getWord();

			if ((w != null) && (!w.equals(""))) {
				if (w.length() > 60) {
					w = safeHTMLString(w);
				}

				int occ = ws.getOccurences();
				String zebra = ((is_odd == 1) ? " zebra" : "");

				data2.append("<div class=\"row occ-values-row");
				data2.append(zebra);
				data2.append("\">\n");
					data2.append("<div class=\"col-xs-12 col-sm-6 occ-value-box\">\n");
						data2.append("<span class=\"occ-value-box-value\">");
						data2.append(w.replaceAll("[^A-Za-z0-9]", ""));
						data2.append("</span>\n");
					data2.append("</div>\n");

					data2.append("<div class=\"col-xs-12 col-sm-6 occ-value-box-v\">\n");
						data2.append("<span class=\"occ-value-box-value-v\">");
						data2.append(occ);
						data2.append("</span>\n");
					data2.append("</div>\n");
				data2.append("</div>\n");

				w = null;
			}
		}

		return data2.toString();
	}

	private static String buildHTMLCode(String today_date, String filename, String para, ArrayList<WordStat> word_list) {
		String f = (((filename != null) && (!filename.equals(""))) ? filename.replaceAll("[^A-Za-z0-9\\.\\-:\\/ ]", "") : "N/A");
		if (f.length() > 60) {
			f = safeHTMLString(f);
		}

		String p = (((para != null) && (!para.equals(""))) ? para.replaceAll("[^A-Za-z0-9\\.\\, ]", "") : "[Empty Text]");
		if (p.length() > 60) {
			p = safeHTMLString(p);
		}

		String td = (((today_date != null) && (!today_date.equals(""))) ? today_date.replaceAll("[^A-Za-z0-9: ]", "") : "N/A");

		StringBuilder data = new StringBuilder();
		data.append("<!DOCTYPE html>\n");
		data.append("<html>\n");
		data.append("<head>\n");
		data.append("<meta charset=\"utf-8\" />\n");
		data.append("<meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\" />\n");
		data.append("<meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />\n");
		data.append("<title>Test Report</title>\n");
		data.append("<script type=\"text/javascript\" src=\"https://code.jquery.com/jquery-3.5.1.min.js\"></script>\n");
		data.append("<script type=\"text/javascript\" src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js\"></script>\n");
		data.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css\" />\n");
		data.append("<style type=\"text/css\">\n");
		data.append(".test-report { margin-bottom:15px }\n");
		data.append(".test-report .buff { border:1px solid #000000;border-radius:5px }\n");
		data.append(".test-report .bigbuff { border:2px solid #000000;border-radius:5px }\n");
		data.append(".test-report .btt { font-weight:bold;font-size:110%;padding-top:5px;padding-bottom:5px }\n");
		data.append(".test-report .date-row { margin-bottom:30px }\n");
		data.append(".test-report .date-row .date-head-box { margin-left:0;margin-right:0 }\n");
		data.append(".test-report .date-row .date-value-box { margin-left:0;margin-right:0;background:#FFFFFF }\n");
		data.append(".test-report .date-row .date-value-box:hover { background:#EEEEEE }\n");
		data.append(".test-report .filename-row { margin-bottom:30px }\n");
		data.append(".test-report .filename-row .filename-head-box { margin-left:0;margin-right:0 }\n");
		data.append(".test-report .filename-row .filename-value-box { margin-left:0;margin-right:0;background:#FFFFFF }\n");
		data.append(".test-report .filename-row .filename-value-box:hover { background:#EEEEEE }\n");
		data.append(".test-report .contents-row { margin-bottom:30px }\n");
		data.append(".test-report .contents-row .contents-box-head { margin-left:0;margin-right:0 }\n");
		data.append(".test-report .contents-row .contents-box-value { margin-left:0;margin-right:0;background:#FFFFFF }\n");
		data.append(".test-report .contents-row .contents-box-value:hover { background:#EEEEEE }\n");
		data.append(".test-report .occ-values-row { background:#FFFFFF;border:1px solid #000000;border-radius:5px }\n");
		data.append(".test-report .occ-values-row.zebra { background:#CCCCCC }\n");
		data.append(".test-report .occ-values-row:hover { background:#EEEEEE;cursor:pointer }\n");
		data.append(".test-report .occ-values-row .occ-value-box-v { padding-top:0 }\n");
		data.append("@media only screen and (max-width:576px) { .test-report .occ-values-row .occ-value-box-v { padding-top:5px }}\n");
		data.append("</style>\n");
		data.append("</head>\n");
		data.append("<body>\n");
		data.append("<div class=\"container test-report\">\n");
			data.append("<div class=\"row date-row\">\n");
				data.append("<div class=\"col-sm-12 btt bigbuff date-head-box\">\n");
					data.append("<span class=\"f-h\">Report Date</span>\n");
				data.append("</div>\n");

				data.append("<div class=\"col-sm-12 buff date-value-box\">\n");
					data.append("<span class=\"f-v\">");
					data.append(td);
					td = null;
					data.append("</span>\n");
				data.append("</div>\n");
			data.append("</div>\n");

			data.append("<div class=\"row filename-row\">\n");
				data.append("<div class=\"col-sm-12 btt bigbuff filename-head-box\">\n");
					data.append("<span class=\"f-h\">Input Filename</span>\n");
				data.append("</div>\n");

				data.append("<div class=\"col-sm-12 buff filename-value-box\">\n");
					data.append("<span class=\"f-v\">");
					data.append(f);
					f = null;
					data.append("</span>\n");
				data.append("</div>\n");
			data.append("</div>\n");

			data.append("<div class=\"row contents-row\">\n");
				data.append("<div class=\"col-sm-12 btt bigbuff contents-box-head\">\n");
					data.append("<span class=\"contents-box-head-value\">File Contents</span>\n");
				data.append("</div>\n");

				data.append("<div class=\"col-sm-12 buff contents-box-value\">\n");
					data.append("<span class=\"cbv\">");
					data.append(p);
					p = null;
					data.append("</span>\n");
				data.append("</div>\n");
			data.append("</div>\n");

			data.append("<div class=\"row btt bigbuff occ-header-row\">\n");
				data.append("<div class=\"col-xs-12 col-sm-6 occ-head\">\n");
					data.append("<span class=\"occ-head-value\">Word</span>\n");
				data.append("</div>\n");

				data.append("<div class=\"col-xs-12 col-sm-6 occ-head\">\n");
					data.append("<span class=\"occ-head-value\">Occurences</span>\n");
				data.append("</div>\n");
			data.append("</div>\n");

			if ((word_list != null) && (word_list.size() > 0)) {
				String temp = null;
				int i = 0;

				for (i = 0; i < word_list.size(); i++) {
					temp = buildStatRow(word_list.get(i), (((i % 2) == 0) ? 0 : 1));
					if (temp != null) {
						data.append(temp);
					}
				}

				temp = null;
			} else {
				data.append("<div class=\"row occ-values-row empty-values-row\">\n");
					data.append("<div class=\"col-xs-12 col-sm-6 occ-value-box\">\n");
						data.append("<span class=\"occ-value-box-value\">N/A</span>\n");
					data.append("</div>\n");

					data.append("<div class=\"col-xs-12 col-sm-6 occ-value-box-v\">\n");
						data.append("<span class=\"occ-value-box-value-v\">N/A</span>\n");
					data.append("</div>\n");
				data.append("</div>\n");
			}
		data.append("</div>\n");
		data.append("</body>\n");
		data.append("<html>\n");

		return data.toString();
	}

	private static String getTodaysDateFormatted() {
		SimpleDateFormat sdf = new SimpleDateFormat("E MMM dd yyyy HH:mm:ss zz");
		Date today = new Date();
		String today_date = "N/A";

		try {
			today_date = sdf.format(today);
		} catch (Exception e) {
			today_date = "N/A";
		}

		sdf = null;
		today = null;

		return today_date;
	}

	public static void main (String[] args) throws IOException {
		System.out.println("PROGRAM START");

		String today_date = getTodaysDateFormatted();
		System.out.println("PROGRAM LOADED ON " + today_date);

		String filepath = "";

		if ((args.length > 0) && (!((args[0]).equals("")))) {
			System.out.println("RETRIEVING PARAGRAPH FILEPATH PARAMETER");
			filepath = (args[0]).trim().replaceAll("[^A-Za-z0-9\\.\\-:\\/ ]", "");
		} else {
			System.out.println("ERROR: MISSING PARAGRAPH FILEPATH PARAMETER\nPROGRAM END");
			System.exit(1);
		}

		int f_len = filepath.length();

		if (f_len < 5) {
			System.out.println("ERROR: INVALID PARAGRAPH FILEPATH REQUESTED\nPROGRAM END");
			System.exit(1);
		}

		System.out.println("PARAGRAPH FILEPATH OK");

		int character_count = 0;
		int i = 0;
		f_len--;

		for (i = 0; i < f_len; i++) {
			if ((filepath.substring(i, (i + 1))).equals(".")) {
				character_count++;
			}
		}

		f_len++;

		if (character_count != 1) {
			System.out.println("ERROR: ILLEGAL PARAGRAPH FILEPATH REQUESTED\nPROGRAM END");
			System.exit(1);
		}

		String filename = "";
		character_count = filepath.lastIndexOf("/");

		if (character_count > -1) {
			filename = filepath.substring(character_count + 1, f_len);
		} else {
			filename = filepath;
		}

		f_len = filename.length();

		if ((f_len < 5) || (!(filename.substring(f_len - 4)).equalsIgnoreCase(".txt"))) {
			System.out.println("ERROR: INVALID PARAGRAPH FILENAME REQUESTED\nPROGRAM END");
			System.exit(1);
		}

		System.out.println("PARAGRAPH FILENAME IS VALID");

		File f_check = null;

		try {
			f_check = new File(filepath);

			if (!f_check.exists()) {
				System.out.println("ERROR: PARAGRAPH FILENAME DOES NOT EXIST IN FILE SYSTEM\nPROGRAM END");
				System.exit(1);
			}
		} catch (Exception e) {
			System.out.println("ERROR CHECKING PARAGRAPH FILENAME: " + e.toString());
			System.exit(1);
		}

		f_check = null;

		System.out.println("PARSING PARAGRAPH FILE START");
		String para = parseTextFile(filepath);
		para = para.replaceAll("[^A-Za-z0-9\\.\\, ]", "");
		System.out.println("PARSING PARAGRAPH FILE END");

		if (character_count > -1) {
			filepath = filepath.substring(0, character_count + 1);
		}

		System.out.println("PARSING WORDS FROM PARAGRAPH START");
		ArrayList<WordStat> word_list = parseWords(para);
		System.out.println("PARSING WORDS FROM PARAGRAPH END");

		if ((word_list != null) && (word_list.size() > 0)) {
			System.out.println("SORTING WORD LIST ALPHABETICALLY START");
			Collections.sort(word_list, new Comparator<WordStat>() {
				@Override
				public int compare(WordStat a, WordStat b) {
					return ((a.getWord()).compareToIgnoreCase(b.getWord()));
				}
			});
			System.out.println("SORTING WORD LIST ALPHABETICALLY END");

			System.out.println("SORTING WORD LIST OCCURENCES START");
			Collections.sort(word_list, new Comparator<WordStat>() {
				@Override
				public int compare(WordStat a, WordStat b) {
					return ((a.getOccurences() == b.getOccurences()) ? 0 : ((a.getOccurences() > b.getOccurences()) ? -1 : 1));
				}
			});
			System.out.println("SORTING WORD LIST OCCURENCES END");
		}

		System.out.println("BUILDING HTML STRING START");
		String report_code = buildHTMLCode(today_date, filename, para, word_list);
		System.out.println("BUILDING HTML STRING END");

		today_date = null;
		filename = null;
		para = null;
		word_list = null;

		if ((report_code == null) || (report_code.equals(""))) {
			System.out.println("ERROR: REPORT HTML CODE NOT FOUND\nPROGRAM END");
			System.exit(1);
		}

		System.out.println("BUILDING REPORT FILEPATH START");
		filepath = filepath + "report.html";
		System.out.println("BUILDING REPORT FILEPATH END");

		System.out.println("WRITING report.html START");
		BufferedWriter writ = new BufferedWriter(new FileWriter(filepath));
		try {
			writ.write(report_code);
		} catch (Exception e) {
			writ.close();
			System.out.println("ERROR WRITING report.html: " + e.toString());
			System.exit(1);
		} finally {
			writ.close();
			System.out.println("WROTE report.html SUCCESSFULLY TO SAME FOLDER AS PARAGRAPH FILE");
		}
		System.out.println("WRITING report.html END");

		filepath = null;
		report_code = null;

		System.out.println("PROGRAM END");
	}
}
